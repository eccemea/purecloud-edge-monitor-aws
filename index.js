// Load the SDK for JavaScript
var AWS = require('aws-sdk');
const nodemailer = require('nodemailer');
const mailObj = require('./config.json');


// Set the region 
AWS.config.update({
    region: 'eu-west-1'
});
const EDGE_TABLE = 'EDGE_Details';
const TOKEN_TABLE = 'PureCloud_Tokens';

// Create the DynamoDB service object
var ddb = new AWS.DynamoDB({
   // apiVersion: '2012-10-08'
});

// First of all, we could delete version for DB.
// Let‚s try

var platformClient = require('purecloud-platform-client-v2'); // PureCloud API 
var client = platformClient.ApiClient.instance;


exports.handler = function (event, context, callback) {
    /*
    console.log('clientId: ', event.clientId);
    console.log('clientSecret: ', event.clientSecret);
    console.log('environment: ', event.environment);
    console.log('edgeId: ', event.edgeId)
    */

    console.log('Process started...');
    client.setEnvironment(event.environment);


    db_getToken(event.clientId, event.environment)
        .then(function (myToken) {
            getEDGEStatus(event.edgeId, event.clientId, event.clientSecret, event.environment, event.customerName, myToken)
                .then(function (resp) {
                    // We have got EDGE Details
                    proceedWithEDGE(resp, event, callback);

                })
                .catch(function (err) {
                    if (err != 401) { // was 401
                        console.log('Stop function!');
                        callback('Failed to query PureCloud API');
                        return
                    }
                    console.log('Try re-login');
                    getEDGEStatus(event.edgeId, event.clientId, event.clientSecret, event.environment, event.customerName)
                        .then(function (resp) {
                            // next steps
                            proceedWithEDGE(resp, event, callback);

                        })
                        .catch(function () {
                            callback('Cannot get EDGE details');
                            return
                        }); // getEDGEStatus re-login without TOKEN

                }); // getEDGEStatus


        })
        .catch(function () {
            callback('Cannot get Token from Database');
            return
        }); // db_getToken


};

function proceedWithEDGE(data, event, callback) {


    return new Promise(function (resolve, reject) {
        var aEdgeId = data.id;
        var aStatus = data.onlineStatus;
        var aEdgeName = data.name;

        console.log('proceedWithEDGE...');

        if (aStatus == 'ONLINE') {
            // EDGE is ONLINE, update it to the DB, to make sure ALERT is disabled

            db_setEDGEValue(aEdgeId, 'ONLINE', event.customerName, aEdgeName)
                .then(function () {
                    callback(null, 'EDGEId: ' + aEdgeId + ' set to ONLINE');
                    return
                })
                .catch(function () {
                    callback('Failed to save EDGE status to the Database');
                    return
                });

        } else {
            // Check if We also know about it
            db_checkEDGEAlert(aEdgeId)
                .then(function (resp) {

                    if (resp) {
                        console.log('Alert for this EDGE is still ACTIVE, do nothing.');
                        callback(null, 'EDGEId: ' + aEdgeId + ' still on ALERT state.');
                        return
                    } else {
                        console.log('EDGE is OFFLINE! Notify Supervisors & Update DB');
                        db_setEDGEValue(aEdgeId, 'OFFLINE', event.customerName, aEdgeName)
                            .then(function () {
                                // SEND MAIL 
                                nodemailer.createTestAccount((err, account) => {

                                    // create reusable transporter object using the default SMTP transport
                                    let transporter = nodemailer.createTransport({
                                        //host: 'smtp.gmail.com',
                                        service: 'SendGrid',
                                        //port: 465,
                                        //secure: true, // true for 465, false for other ports
                                        
                                        auth: {
                                            user: mailObj.userMail, // generated ethereal user MSC Test Account
                                            pass: mailObj.userPassword // generated ethereal password
                                        }
                                        
                                    });

                                    // setup email data with unicode symbols
                                    let mailOptions = {
                                        from: 'edgemonitoring@sendgrid.net', // '"Fred Foo 👻" <anonim@gmail.com>', // sender address
                                        to: event.smtpTo, //'daniel.szlaski@me.com', // list of receivers
                                        subject: 'EDGE (' + aEdgeName + ') is Offline!', // Subject line
                                        text: 'Alert ! \nPureCloud EDGE (' + aEdgeId + ') is Offline!' + ' \nOrgName: ' + event.customerName + '\nDescription:  ' + event.description, // plain text body
                                        html: '<center><table bgcolor="white" cellpadding="0" border="0"><tr><td><img width=250px src="http://go.genesys.com/rs/426-TDW-681/images/170614_Genesys_top-hdr_v2.jpg" /></td></tr><tr><td><img src="http://go.genesys.com/rs/426-TDW-681/images/PureCloud-Warm-Red-EH-EN.png" /><h2><font color="#4e5054"><br>PureCloud EDGE (' + aEdgeName + ') is Offline!</h2>EDGE Id: ' + aEdgeId + ' <br><br>OrgName: ' + event.customerName + '<br>Description:  ' + event.description  + '</font></td></tr><tr bgcolor="white" height="100"><td></td></tr><tr bgcolor="#4e5054" height="50"><td><center><font size="2" color="white">PureCloud EDGE Monitoring Service</font></center></td></tr></table></center>'
                                    };

                                    // send mail with defined transport object
                                    transporter.sendMail(mailOptions, (error, info) => {
                                        if (error) {
                                            console.log(error);
                                            callback(error);
                                            return
                                        }
                                        console.log('Message sent: %s', info.messageId);
                                        callback(null, 'Message sent: ' + info.messageId);

                                    });
                                });
                            })
                            .catch(function (err) {
                                console.log(err);
                                callback('Error during update EDGE Alert');

                            });
                    }

                })
                .catch(function (err) {
                    console.log(err);
                    callback('Error during db_checkEDGEAlert');
                });
        }

    });

}

function getEDGEStatus(aEdgeId, aClientId, aClientSecret, aEnv, aName, aToken) {
    console.log('getEDGEStatus...');

    return new Promise(function (resolve, reject) {

        if (aToken) {
            // Use Token
            client.setAccessToken(aToken);
            var apiInstance = new platformClient.TelephonyProvidersEdgeApi();
            var opts = {};

            console.log('PC API Function: getTelephonyProvidersEdge...');
            apiInstance.getTelephonyProvidersEdge(aEdgeId, opts)
                .then(function (data) {
                    console.log('EDGE: ' + data.id + ': ' + data.onlineStatus);
                    resolve(data);
                })
                .catch(function (err) {
                    console.log('Error during getTelephonyProvidersEdge with Token:  ', err.status);
                    reject(err.status);

                }); //getTelephonyProvidersEdge

        } else {
            // Log-in
            client.loginClientCredentialsGrant(aClientId, aClientSecret)
                .then(function () {
                    // Do authenticated things
                    console.log('Service logged-in...');
                    aToken = client.authentications["PureCloud Auth"].accessToken;
                    // SAVE IT TO DB First !!!!
                    db_setToken(aClientId, aToken, aName)
                        .then(function (data) {
                            console.log('New Token Saved to the Database');
                            var apiInstance = new platformClient.TelephonyProvidersEdgeApi();
                            var opts = {};

                            console.log('PC API Function: getTelephonyProvidersEdge...');
                            apiInstance.getTelephonyProvidersEdge(aEdgeId, opts)
                                .then(function (data) {
                                    console.log('EDGE: ' + data.id + ': ' + data.onlineStatus);
                                    resolve(data);
                                })
                                .catch(function (err) {
                                    console.log('Error during getTelephonyProvidersEdge:  ', err.status);
                                    reject();
                                }); // getTelephonyProvidersEdge

                        })
                        .catch(function (err) {
                            console.log(err);
                            reject();
                        }); //db_setToken

                })
                .catch(function (err) {
                    console.log('Error during loginClientCredentialsGrant:  ', err.status);
                    reject();
                });
        }
    });
}

function db_checkEDGEAlert(aId) {

    return new Promise(function (resolve, reject) {
        console.log('db_checkEDGEAlert: ', aId);
        var params = {
            TableName: EDGE_TABLE,
            Key: {
                'Id': {
                    S: aId
                },
            },
            ProjectionExpression: 'onlineStatus'
        };


        try {
            // Call DynamoDB to read the item from the table
            ddb.getItem(params, function (err, data) {
                if (err) {
                    console.log("getItem Error", err);
                    reject(false);
                } else {
                    // We have got results from DB

                    if (!Object.keys(data).length) {
                        console.log('EDGE not found.');
                        resolve(false);
                        return

                    }

                    if (data.Item['onlineStatus'].S == 'OFFLINE') {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            });
        } catch (error) {

            reject(false);
        }

    });

}

function db_setEDGEValue(aId, aValue, aName, aEdgeName) {

    return new Promise(function (resolve, reject) {

        console.log('db_setEDGEValue (' + aValue + ')');
        var params = {
            TableName: EDGE_TABLE,
            Item: {
                'Id': {
                    S: aId
                },
                'edgeName': {
                    S: aEdgeName
                },
                'customerName': {
                    S: aName
                },
                'onlineStatus': {
                    S: aValue
                },
            }
        };

        // Call DynamoDB to add the item to the table
        ddb.putItem(params, function (err, data) {
            if (err) {
                console.log("putItem Error", err);
                reject();
            } else {
                //console.log("Success", data);
                console.log("putItem Success");
                resolve();
            }
        });

    });

}

function db_getToken(aClientId, aEnv) {

    return new Promise(function (resolve, reject) {
        console.log('db_getToken: ', aClientId);
        var params = {
            TableName: TOKEN_TABLE,
            Key: {
                'clientId': {
                    S: aClientId
                }
            },
            ProjectionExpression: 'sToken'
        };


        try {
            // Call DynamoDB to read the item from the table
            ddb.getItem(params, function (err, data) {
                if (err) {
                    console.log("getItem Error", err);
                    reject(false);
                } else {
                    // We have got results from DB
                    if (!Object.keys(data).length) {
                        console.log('Token not present.');
                        resolve(false);
                        return

                    }
                    resolve(data.Item['sToken'].S);
                }
            });
        } catch (error) {
            console.log(error);
            reject(false);
        }

    });

}

function db_setToken(aClientId, aToken, aName) {

    return new Promise(function (resolve, reject) {

        console.log('db_setToken for clientId (' + aClientId + ')');
        var params = {
            TableName: TOKEN_TABLE,
            Item: {
                'clientId': {
                    S: aClientId
                },
                'customerName': {
                    S: aName
                },
                'sToken': {
                    S: aToken
                }
            }
        };

        // Call DynamoDB to add the item to the table
        ddb.putItem(params, function (err, data) {
            if (err) {
                console.log("putItem Error", err);
                reject();
            } else {
                //console.log("Success", data);
                console.log("putItem Success");
                resolve();
            }
        });

    });

}