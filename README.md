# PureCloud Edge Monitoring Service (AWS version)

AWS Lambda Function that monitor (polling) status for defined EDGES.
All Email notifications are sent from dedicated mail platform.



## Local Installation & Testing 

Use SAM CLI For Mac / Windows

* Install docker first.
* $ npm install --global aws-sam-local

## Test
To test application locally, use:

$ sam local invoke "test" -e event.json